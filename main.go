package main

import (
	"fmt"
)

/*var wg sync.WaitGroup

func hardTask(name string) {
	defer wg.Done()

	for i := 0; i < 10; i++ {
		// time.Sleep(1 + time.Second)
		fmt.Printf("Hard task %s...\n", name)
	}
	fmt.Printf("Hard task %s DONE\n", name)
}*/

/*func task(done chan bool) {
	fmt.Println("START TASK")
	time.Sleep(time.Second * 3)
	fmt.Println("END TASK")

	done <- true
}*/

func sendMessage(msg chan string) {
	msg <- "Hello go"
}

func receiveMessage(msg chan string, done chan bool) {
	fmt.Println(<-msg)

	done <- true
}

func main() {

	done := make(chan bool)
	msg := make(chan string)

	go sendMessage(msg)
	go receiveMessage(msg, done)

	<-done

	/*done := make(chan bool)
	go task(done)
	<-done*/

	/*for i := 0; i < 10; i++ {
		wg.Add(1)
		go hardTask(strconv.Itoa(i))
	}

	fmt.Println("FINISH")

	wg.Wait()*/

}

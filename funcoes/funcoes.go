package funcoes

// Soma is a function that does a sum
func Soma(num1, num2 int) int {
	return num1 + num2
}

func Divisao(num1, num2 int) (int, bool) {
	if num2 == 0 {
		return 0, false
	}
	return num1 / num2, true
}

func Subtracao(num1, num2 int) int {
	return num1 - num2
}

func SomaNumeros(numeros ...int) int {
	var total = 0

	for _, valor := range numeros {
		total += valor
	}

	return total
}

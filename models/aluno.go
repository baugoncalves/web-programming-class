package models

import (
	"fmt"
	"strings"
)

type Aluno struct {
	Name      string
	Sobrenome string
	Idade     int
}

func ImprimirAluno(name string, idade int) {
	var aluno Aluno

	aluno.Name = name
	aluno.Idade = idade
	fmt.Println(aluno.Name, aluno.Idade)
}

func (a Aluno) NomeCompleto() {
	a.Name = "amaury"

	fmt.Println("na funcao", strings.ToUpper(a.Name))
	// return a.Name + " " + a.Sobrenome
}

func (a *Aluno) CapitalizarNome() {
	a.Name = "fulano"
	a.Name = strings.ToUpper(a.Name)
}

package interfaces

import (
	"fmt"
	"math"
)

type Geometria interface {
	area() float64
}

type Quadrado struct {
	lado float64
}

type Circulo struct {
	raio float64
}

func (q Quadrado) area() float64 {
	return q.lado * q.lado
}

func (c Circulo) area() float64 {
	return math.Pi * (c.raio * c.raio)
}

func Resultados() {
	var g Geometria

	g = Quadrado{3}

	fmt.Println("A area é:", g.area())

	g = Circulo{5}

	fmt.Println("A area do circulo:", g.area())
}
